import Responder from '../../lib/expressResponder'
import request from '../../lib/request'
import Express from '../../lib/express';
import { Event } from '../models';
const bodyParser = require("body-parser");
export default class EventController {
  static listEvent(req, res) {
      Event.find()
    .then(event => Responder.success(res, event))
    .catch(errorOnDBOp => Responder.operationFailed(res, errorOnDBOp));
     
    }

 static createEvent(req, res) {
    let reqData = req.body;
    console.log('**************************', reqData);
      Event.create(reqData)
    .then(plans => Responder.success(res, plans))
    .catch(errorOnDBOp => Responder.operationFailed(res, errorOnDBOp));
  }

}
