import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const EventSchema = new Schema({
    title: {
      type: String,
      trim: true,
      required: true
    },
    description: {
      type: String,
      trim: true,
      default: ''
    },
    place: {
      type: String,
      trim: true,
      required: true
    },
    lat: {
      type: String,
      trim: true,
      required: true
    },
    lng: {
      type: String,
      trim: true,
      required: true
    },
    start: {
      type: String,
      default: true
    },
    end: {
      type: String,
      default: false
    }
  
});

export default mongoose.model('Event', EventSchema);

