import initEventRoutes from './eventRoutes';

const initRoutes = (app) => {
  app.use(`/`, initEventRoutes());
};

export default initRoutes;
