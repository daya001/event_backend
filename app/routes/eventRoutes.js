import express from 'express';
import EventController from '../controllers/eventController';


const initEventRoutes = () => {
	const eventRoutes = express.Router();
	eventRoutes.get('/events', EventController.listEvent);
	eventRoutes.post('/create', EventController.createEvent);
	return eventRoutes;
};

export default initEventRoutes;
